﻿#include <SFML/Graphics.hpp>

#include <Constants.h>
#include <Visualizer.h>


int main()
{

    sf::RenderWindow window({ gridWidth, gridHeight }, "Title", sf::Style::Titlebar | sf::Style::Close);

    Visualizer visualizer(window);
    visualizer.configureWindow();

    visualizer.createGrid();
    sf::Clock clock;

    // Loop
    visualizer.reset();
    while (visualizer.window().isOpen())
    {
        visualizer.inputHandler();

        if (visualizer.isRunning() && !visualizer.isDestFound())
        {
            // Slow down the pathfinder algorithm for better visualization
            sf::Time time = clock.getElapsedTime();
            bool timePassed = time.asMilliseconds() >= visualizer.animationSpeed();
            if (timePassed)
            {
                visualizer.tickPathProgress();
                clock.restart();
            }
        }
        visualizer.displayScene();
    }
    visualizer.reset();
}