#ifndef NODE_H
#define NODE_H
#pragma once

#include <DataStructures.h>

/// <summary>
/// A class the represents a basic node for pathfinding.
/// </summary>
class Node
{
public:

    /// <summary>
    /// Creates a Node object with x, y coordinates and an optional parent Node.
    /// </summary>
    /// <param name="coordinates">Location of the node.</param>
    /// <param name="parent">Optional parent Node.</param>
    /// <returns>A Node object.</returns>
    Node(const Coordinate2D&, Node*);

    /// <summary>
    /// Change or set the parent Node.
    /// </summary>
    /// <param name="parent">A pointer to the parent Node.</param>
    void setParent(Node*);

    /// <summary>
    /// Gets the parent Node.
    /// </summary>
    /// <returns>A pointer to the parent Node.</returns>
    Node* getParent() const;

    /// <summary>
    /// Change the coordinates of the current Node.
    /// </summary>
    /// <param name="coordinates">A structure containing x and y coordinates.</param>
    void setCoordinates(Coordinate2D);

    /// <summary>
    /// Get the coordinates of the current Node.
    /// </summary>
    /// <returns>A structure containing x and y coordinates.</returns>
    Coordinate2D getCoordinates() const;

protected:
    Node* m_parent;
    Coordinate2D m_coordinates;
};

#endif // !NODE_H