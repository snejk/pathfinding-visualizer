#include <PathfinderBase.h>
#include <Pathfinder.h>
#include <Node.h>
#include <Dijkstra.h>

GraphNode::GraphNode(const Coordinate2D& coordinates, Node* parent = nullptr) : Node(coordinates, parent)
{
    
};

unsigned int GraphNode::getDistance() const
{
    return m_distance;
}

void GraphNode::setDistance(unsigned int distance)
{
    m_distance = distance;
}

Dijkstra::Dijkstra(bool diagonalMovement) : Pathfinder::Pathfinder(diagonalMovement)
{
    
}

Dijkstra::~Dijkstra()
{
    clear();
}

sf::String Dijkstra::getName()
{
    return sf::String("Dijkstra/BFS Visualization");
}

void Dijkstra::explorePath(const BlockMap& wallBlocks)
{
    if (!m_openSet.empty())
    {
        m_current = *m_openSet.begin();

        for (GraphNode* node : m_openSet)
        {
            if (node->getDistance() <= m_current->getDistance())
                m_current = node;
        }

        m_closedSet.insert(m_current);
        m_openSet.erase(m_current);

        if (m_current->getCoordinates() == m_dest)
        {
            m_isDestFound = true;
            return;
        }

        // Explore neighbours
        for (auto i = 0; i < m_directionCount; ++i)
        {
            Coordinate2D neighbourPos = m_current->getCoordinates() + directions[i];
            auto compareFunc = [&neighbourPos](const Node* node) { return node->getCoordinates() == neighbourPos; };

            auto closedSetIterator = std::find_if(m_closedSet.begin(), m_closedSet.end(), compareFunc);

            if (!isTraversable(neighbourPos, wallBlocks) || closedSetIterator != m_closedSet.end())
                continue;

            // Orthogonal movement is represented by a coordinate whose x or y are equal to 0
            unsigned int distance = m_current->getDistance() + ((directions[i].x == 0 || directions[i].y == 0) ? orthogonalCost : diagonalCost);

            auto openSetIterator = std::find_if(m_openSet.begin(), m_openSet.end(), compareFunc);

            if (openSetIterator == m_openSet.end())
            {
                GraphNode* neighbour = new GraphNode(neighbourPos, m_current);
                neighbour->setDistance(distance);
                m_openSet.insert(neighbour);
            }
            else if (distance < (*openSetIterator)->getDistance())
            {
                (*openSetIterator)->setParent(m_current);
                (*openSetIterator)->setDistance(distance);
            }
        }
    }
}

void Dijkstra::setSource(Coordinate2D newSource)
{
    clear();
    PathfinderBase::setSource(newSource);
    m_openSet.clear();
    m_openSet.insert(new GraphNode(m_source));
}