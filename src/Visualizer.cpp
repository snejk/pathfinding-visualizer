#include <Visualizer.h>

Visualizer::Visualizer(sf::RenderWindow& window) : m_window(window)
{
    m_drawClosedBlocksFunc = [&](Coordinate2D nodeCoords)
    {
        Block block = { nodeCoords.x, nodeCoords.y, sf::Color{ 127, 210, 127 }, sf::Color{ 127, 127, 127 } };
        m_window.draw(block.m_shape);
    };
    m_algorithmList.push_back(new AStar(true));
    m_algorithmList.push_back(new Dijkstra(true));
    m_algorithmList.push_back(new DFS(false));

    m_algorithmIterator = m_algorithmList.begin();
    m_window.setTitle((*m_algorithmIterator)->getName());

    m_animationSpeed = 100.f;
}

Visualizer::~Visualizer()
{
    for (auto* it : m_algorithmList)
        delete it;
    m_algorithmList.clear();
}

const sf::RenderWindow& Visualizer::window() const
{
    return m_window;
}

const bool Visualizer::isRunning() const
{
    return m_running;
}

const float Visualizer::animationSpeed() const
{
    return m_animationSpeed;
}

void Visualizer::inputHandler()
{
    sf::Event event;
    while (m_window.pollEvent(event))
    {
        switch (event.type)
        {
            case sf::Event::Closed:
            {
                m_window.close();
                break;
            }
            case sf::Event::KeyPressed:
            {
                // Start the visualization
                if (event.key.code == sf::Keyboard::G)
                {
                    handleStart();
                }
                // Cycle through algorithms
                else if (event.key.code == sf::Keyboard::A)
                {
                    if (!m_running)
                        handleAlgorithmChange();
                }
                // Reset
                else if (event.key.code == sf::Keyboard::R)
                {
                    reset();
                }
                // Close the application
                else if (event.key.code == sf::Keyboard::Escape)
                {
                    m_window.close();
                }
                // Set source and destination blocks
                else if (event.key.code == sf::Keyboard::S || event.key.code == sf::Keyboard::D)
                {
                    if (!m_running)
                        handleSourceDest(event);
                }
                // Speed up or slow down the visualization
                else if (event.key.code == sf::Keyboard::Up || event.key.code == sf::Keyboard::Down)
                {
                    handleAnimationSpeedChange(event);
                }
                break;
            }
            default:
                break;
        }
    }

    // Outside event loop to handle holding down the left mouse button to draw collision walls
    if (sf::Mouse::isButtonPressed(sf::Mouse::Left) || sf::Mouse::isButtonPressed(sf::Mouse::Right))
    {
        handleWallBuilder();
    }
}

void Visualizer::handleStart()
{
    // Only run the visualizer if both source and destionation blocks are defined
    if ((*m_algorithmIterator)->getSource() >= 0 && (*m_algorithmIterator)->getDest() >= 0)
    {
        m_running = true;
    }
}

void Visualizer::handleAlgorithmChange()
{
    reset();
    ++m_algorithmIterator;
    if (m_algorithmIterator == m_algorithmList.end())
        m_algorithmIterator = m_algorithmList.begin();
    m_window.setTitle((*m_algorithmIterator)->getName());
}

void Visualizer::handleSourceDest(sf::Event event)
{
    auto mousePos = sf::Mouse::getPosition(m_window);
    const int x = (int) (mousePos.x / blockWidth);
    const int y = (int) (mousePos.y / blockHeight);
    auto coord = Coordinate2D(x, y);

    // Can't put a source/destination block on a wall block
    if (m_wallBlocks.count(coord))
        return;

    const std::unordered_map<Coordinate2D, Block, hashFunc, equalFunc>::const_iterator it = m_gridBlocks.find(coord);
    if (it != m_gridBlocks.end() && it->second.m_shape.getGlobalBounds().contains(m_window.mapPixelToCoords(mousePos)))
    {
        if (event.key.code == sf::Keyboard::S)
        {
            Block block = { x, y, sf::Color::Yellow, sf::Color{ 127, 127, 127 } };
            m_sourceBlock = { Coordinate2D(x, y), block };
            (*m_algorithmIterator)->setSource(Coordinate2D(x, y));
        }
        else if (event.key.code == sf::Keyboard::D)
        {
            Block block = { x, y, sf::Color::Red, sf::Color{ 127, 127, 127 } };
            m_destBlock = { Coordinate2D(x, y), block };
            (*m_algorithmIterator)->setDest(Coordinate2D(x, y));
        }
    }
}

void Visualizer::handleWallBuilder()
{
    auto mousePos = sf::Mouse::getPosition(m_window);
    const int x = (int) (mousePos.x / blockWidth);
    const int y = (int) (mousePos.y / blockHeight);
    auto coord = Coordinate2D(x, y);

    // Can't put a wall on a source/destionation block
    if ((*m_algorithmIterator)->getSource() == coord || (*m_algorithmIterator)->getDest() == coord)
        return;

    const std::unordered_map<Coordinate2D, Block, hashFunc, equalFunc>::const_iterator it = m_gridBlocks.find(coord);
    if (it != m_gridBlocks.end() && it->second.m_shape.getGlobalBounds().contains(m_window.mapPixelToCoords(mousePos)))
    {
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            Block block = { x, y, sf::Color::Black, sf::Color::Black };
            m_wallBlocks.insert({ Coordinate2D(x, y), block });
        }
        else if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
        {
            if (m_wallBlocks.count(coord))
            {
                m_wallBlocks.erase(m_wallBlocks.find(coord));
            }
        }
    }
}

void Visualizer::handleAnimationSpeedChange(sf::Event event)
{
    // Cap the minimum and maximum animation speed
    auto const minSpeed = 150.f;
    auto const maxSpeed = 10.f;
    auto const speedStep = 10.f;

    // Increase the animation speed
    if (event.key.code == sf::Keyboard::Up)
    {
        if (m_animationSpeed < maxSpeed)
        {
            m_animationSpeed = maxSpeed;
        }
        else
        {
            m_animationSpeed -= speedStep;
        }
    }
    // Decrease the animation speed
    else if (event.key.code == sf::Keyboard::Down)
    {
        if (m_animationSpeed > minSpeed)
        {
            m_animationSpeed = minSpeed;
        }
        else
        {
            m_animationSpeed += speedStep;
        }
    }
}

void Visualizer::createGrid()
{
    for (int x = 0; x < xBlockCount; ++x)
    {
        for (int y = 0; y < yBlockCount; ++y)
        {
            Block block = { x, y, sf::Color{ 200, 200, 200 }, sf::Color{ 127, 127, 127 } };
            m_gridBlocks.insert({ Coordinate2D(x, y), block });
        }
    }
}

void Visualizer::displayScene()
{
    // Draw default grid blocks
    for (const auto& block : m_gridBlocks)
    {
        m_window.draw(block.second.m_shape);
    }

    // Draw wall blocks
    for (const auto& block : m_wallBlocks)
    {
        m_window.draw(block.second.m_shape);
    }

    // Draw closed nodes
    (*m_algorithmIterator)->enumerateClosedNodeCoords(m_drawClosedBlocksFunc);

    // Draw current node
    Coordinate2D currentNode = (*m_algorithmIterator)->getCurrentNodeCoords();
    if (currentNode >= 0)
    {
        Block block = { currentNode.x, currentNode.y, sf::Color::Blue, sf::Color::Black };
        m_window.draw(block.m_shape);
    }

    // Draw found path
    if ((*m_algorithmIterator)->isDestFound())
    {
        auto path = (*m_algorithmIterator)->getReconstructedPath();
        sf::VertexArray lines(sf::LinesStrip, path.size());

        for (auto i = 0; i < path.size(); i++)
        {
            auto coordinate = path[i];

            lines[i].position = sf::Vector2f(coordinate.x * blockWidth + (blockWidth / 2), coordinate.y * blockHeight + (blockHeight / 2));
            lines[i].color = sf::Color{ 26, 0, 30 };

            Block block = { coordinate.x, coordinate.y, sf::Color{ 147, 147, 250 }, sf::Color{ 127, 127, 127 } };
            m_window.draw(block.m_shape);
        }
        m_window.draw(lines);
        m_running = false;
    }

    // Draw source and destination blocks and display the scene
    m_window.draw(m_sourceBlock.second.m_shape);
    m_window.draw(m_destBlock.second.m_shape);
    m_window.display();
}

void Visualizer::tickPathProgress()
{
    (*m_algorithmIterator)->explorePath(m_wallBlocks);
}

void Visualizer::reset()
{
    (*m_algorithmIterator)->clear();
    m_wallBlocks.clear();
    m_pathBlocks.clear();
    m_sourceBlock = BlockPair();
    m_destBlock = BlockPair();
    m_running = false;
    m_window.clear();
}

void Visualizer::configureWindow()
{
    sf::FloatRect viewport(0.f, 0.f, 1.f, 1.f);
    auto windowSize = m_window.getSize();
    sf::View view(sf::FloatRect(0.f, 0.f, (float) (windowSize.x), (float) (windowSize.y)));
    view.setViewport(viewport);

    m_window.setView(view);
    m_window.setFramerateLimit(60);
}

bool Visualizer::isDestFound()
{
    return (*m_algorithmIterator)->isDestFound();
}