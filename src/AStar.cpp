#include <cmath>

#include <PathfinderBase.h>
#include <Pathfinder.h>
#include <Node.h>
#include <AStar.h>

AStarNode::AStarNode(const Coordinate2D& coordinates, Node* parent = nullptr) : Node(coordinates, parent)
{

};

unsigned int AStarNode::F() const
{
    return m_G + m_H;
}

unsigned int AStarNode::getG() const
{
    return m_G;
}

void AStarNode::setG(unsigned int g)
{
    m_G = g;
}

unsigned int AStarNode::getH() const
{
    return m_H;
}

void AStarNode::setH(unsigned int h)
{
    m_H = h;
}

AStar::AStar(bool diagonalMovement) : Pathfinder::Pathfinder(diagonalMovement)
{

};

AStar::~AStar()
{
    clear();
}

sf::String AStar::getName()
{
    return sf::String("A* Visualization");
}

void AStar::explorePath(const BlockMap& wallBlocks)
{
    if (!m_openSet.empty())
    {
        m_current = *m_openSet.begin();

        // Find node in the open set with lowest f value
        // Use it as the current node
        for (auto node : m_openSet)
        {
            if (node->F() <= m_current->F())
            {
                m_current = node;
            }
        }

        m_closedSet.insert(m_current);
        m_openSet.erase(m_current);

        // Path found, return early
        if (m_current->getCoordinates() == m_dest)
        {
            m_isDestFound = true;
            return;
        }

        // Explore neighbours
        for (auto i = 0; i < m_directionCount; ++i)
        {
            Coordinate2D neighbourPos = m_current->getCoordinates() + directions[i];
            auto compareFunc = [&neighbourPos](const Node* node) { return node->getCoordinates() == neighbourPos; };

            auto iterClosedSet = std::find_if(m_closedSet.begin(), m_closedSet.end(), compareFunc);

            // Skip non-traversable grid blocks (wall collision or end of the grid) and the current node
            if (!isTraversable(neighbourPos, wallBlocks) || iterClosedSet != m_closedSet.end())
                continue;

            // Orthogonal movement is represented by a coordinate whose x or y are equal to 0
            unsigned int G = m_current->getG() + ((directions[i].x == 0 || directions[i].y == 0) ? orthogonalCost : diagonalCost);

            auto iterOpenSet = std::find_if(m_openSet.begin(), m_openSet.end(), compareFunc);

            // Add a new node that's connected to the current node if it's not found in the open set
            if (iterOpenSet == m_openSet.end())
            {
                AStarNode* neighbour = new AStarNode(neighbourPos, m_current);
                neighbour->setG(G);
                neighbour->setH(manhattan(neighbour->getCoordinates(), m_dest));
                m_openSet.insert(neighbour);
            }
            // Otherwise update the parent and the cost
            else if (G < (*iterOpenSet)->getG())
            {
                (*iterOpenSet)->setParent(m_current);
                (*iterOpenSet)->setG(G);
            }
        }
    }
}

void AStar::setSource(Coordinate2D newSource)
{
    clear();
    PathfinderBase::setSource(newSource);
    m_openSet.clear();
    m_openSet.insert(new AStarNode(m_source));
}

std::pair<int, int> AStar::calcDelta(const Coordinate2D& src, const Coordinate2D& dest)
{
    return std::make_pair(abs(src.x - dest.x), abs(src.y - dest.y));
}

int AStar::manhattan(const Coordinate2D& source, const Coordinate2D& dest)
{
    auto delta = calcDelta(source, dest);
    return orthogonalCost * (int) (delta.first + delta.second);
}

int AStar::euclidean(const Coordinate2D& source, const Coordinate2D& dest)
{
    auto delta = calcDelta(source, dest);
    return orthogonalCost * (int) std::sqrt(std::pow(delta.first, 2) + std::pow(delta.second, 2));
}

int AStar::octagonal(const Coordinate2D& source, const Coordinate2D& dest)
{
    auto delta = calcDelta(source, dest);
    return orthogonalCost * (int) (delta.first + delta.second) + (-6 * std::min(delta.first, delta.second));
}