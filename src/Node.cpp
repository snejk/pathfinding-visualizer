#include <Node.h>

#include <DataStructures.h>


Node::Node(const Coordinate2D& coordinates, Node* parent = nullptr) : m_coordinates(coordinates), m_parent(parent) {};

void Node::setParent(Node* parent)
{
    m_parent = parent;
}

Node* Node::getParent() const
{
    return m_parent;
}

void Node::setCoordinates(Coordinate2D coordinates)
{
    m_coordinates = coordinates;
}

Coordinate2D Node::getCoordinates() const 
{
    return m_coordinates;
}