#ifndef DFS_H
#define DFS_H
#pragma once

#include <PathfinderBase.h>
#include <Pathfinder.h>
#include <Node.h>

#include <stack>

/// <summary>
/// A class that represents the depth first search pathfinding algorithm.
/// </summary>
class DFS : public Pathfinder<Node>
{
public:

    /// <summary>
    /// Instantiates a depth first search algorithm object.
    /// </summary>
    /// <param name="diagonalMovement">Enable the diagonal movement for the algorithm.</param>
    /// <returns>And object representing the depth first search algorithm.</returns>
    DFS(bool);

    ~DFS();

    /// <summary>
    /// Get the name of the current algorithm.
    /// </summary>
    /// <returns>The current algorithm's name.</returns>
    sf::String getName() override;

    /// <summary>
    /// The main method of the algorithm. Explores neighbours to find the path to the destination.
    /// </summary>
    /// <param name="wallBlocks">A map of wall grid blocks.</param>
    void explorePath(const BlockMap&) override;

    /// <summary>
    /// Sets the source block's coordinates.
    /// </summary>
    /// <param name="newSource">Coordinates of the new source block.</param>
    void setSource(Coordinate2D) override;

    /// <summary>
    /// Free the memory and set the private members to default.
    /// </summary>
    void clear() override;

private:
    std::stack<Node*> m_openSet;
};

#endif // !DFS_H