#include <DFS.h>

DFS::DFS(bool diagonalMovement) : Pathfinder::Pathfinder(diagonalMovement)
{

}

DFS::~DFS()
{
    clear();
}

sf::String DFS::getName()
{
    return sf::String("DFS Visualization");
}

void DFS::explorePath(const BlockMap& wallBlocks)
{
    if (!m_openSet.empty())
    {
        m_current = (Node*) m_openSet.top();
        m_openSet.pop();

        m_closedSet.insert(m_current);

        if (m_current->getCoordinates() == m_dest)
        {
            m_isDestFound = true;
            return;
        }

        // Explore neighbours
        for (auto i = 0; i < m_directionCount; ++i)
        {
            Coordinate2D neighbourPos = m_current->getCoordinates() + directions[i];
            auto compareFunc = [&neighbourPos](const Node* node) { return node->getCoordinates() == neighbourPos; };

            auto closedSetIterator = std::find_if(m_closedSet.begin(), m_closedSet.end(), compareFunc);

            if (!isTraversable(neighbourPos, wallBlocks) || closedSetIterator != m_closedSet.end())
                continue;

            Node* neighbour = new Node(neighbourPos, m_current);
            m_openSet.push(neighbour);
        }
    }
}

void DFS::clear()
{
    while (!m_openSet.empty())
    {
        auto* it = m_openSet.top();
        delete it;
        m_openSet.pop();
    }

    for (auto it = m_closedSet.begin(); it != m_closedSet.end();) {
        delete* it;
        it = m_closedSet.erase(it);
    }

    m_source = Coordinate2D(-1, -1);
    m_dest = Coordinate2D(-1, -1);

    m_current = nullptr;
    m_path.clear();
    m_isDestFound = false;
}

void DFS::setSource(Coordinate2D newSource)
{
    clear();
    PathfinderBase::setSource(newSource);
    while (!m_openSet.empty())
    {
        m_openSet.pop();
    }    
    m_openSet.push(new Node(m_source, nullptr));
}