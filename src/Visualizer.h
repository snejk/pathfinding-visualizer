#ifndef VISUALIZER_H
#define VISUALIZER_H
#pragma once

#include <SFML/Graphics.hpp>

#include <Constants.h>
#include <DataStructures.h>
#include <Dijkstra.h>
#include <AStar.h>
#include <DFS.h>

#include <vector>
#include <functional>

/// <summary>
/// A class that handles the animation of pathfinding algorithms.
/// </summary>
class Visualizer
{
public:

    /// <summary>
    /// Constructs the visualizer object.
    /// </summary>
    /// <param name="window">An SFML RenderWindow object.</param>
    Visualizer(sf::RenderWindow&);
    ~Visualizer();

    /// <summary>
    /// Get the SFML RenderWindow object.
    /// </summary>
    /// <returns>The SFML RenderWindow object reference.</returns>
    const sf::RenderWindow& window() const;

    /// <summary>
    /// Returns true if the animation is running.
    /// </summary>
    /// <returns>Whether the animation is running.</returns>
    const bool isRunning() const;

    /// <summary>
    /// Returns speed at which the animation should be running.
    /// </summary>
    /// <returns>A number representing the animation speed.</returns>
    const float animationSpeed() const;

    /// <summary>
    /// Handles all the keyboard and mouse inputs.
    /// </summary>
    void inputHandler();

    /// <summary>
    /// Create the initial blank grid.
    /// </summary>
    void createGrid();

    /// <summary>
    /// Draws source and destination blocks, explored path
    /// and finally the path between the source and the destination.
    /// </summary>
    void displayScene();

    /// <summary>
    /// Perform one step of the pathfinding algorithm.
    /// </summary>
    void tickPathProgress();

    /// <summary>
    /// Reset the whole grid to its initial state.
    /// </summary>
    void reset();

    /// <summary>
    /// Configure the SFML RenderWindow object.
    /// Sets the viewport and framerate limit.
    /// </summary>
    void configureWindow();

    /// <summary>
    /// Checks if the path between the source and the destination has been found.
    /// </summary>
    /// <returns>True if the destination block has been found.</returns>
    bool isDestFound();

private:
    bool m_running = false;
    sf::RenderWindow& m_window;
    sf::Vector2u m_windowSize;
    BlockMap m_gridBlocks;
    BlockMap m_wallBlocks;
    BlockMap m_pathBlocks;
    BlockPair m_sourceBlock;
    BlockPair m_destBlock;
    float m_animationSpeed;

    std::vector<PathfinderBase*> m_algorithmList;
    decltype(m_algorithmList)::iterator m_algorithmIterator;
    std::function<void(Coordinate2D)> m_drawClosedBlocksFunc;

    void handleStart();
    void handleAlgorithmChange();
    void handleSourceDest(sf::Event);
    void handleWallBuilder();
    void handleAnimationSpeedChange(sf::Event);
};

#endif // !VISUALIZER_H