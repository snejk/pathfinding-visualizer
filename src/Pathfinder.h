#ifndef PATHFINDER_H
#define PATHFINDER_H
#pragma once

#include <Constants.h>
#include <DataStructures.h>

/// <summary>
/// Main class for implementing pathfinding algorithms.
/// </summary>
/// <typeparam name="TNode">The type of nodes that the algorithms uses. Should be inherited from Node.</typeparam>
template<class TNode>
class Pathfinder : public PathfinderBase
{
public:
    /// <summary>
    /// Default constructor.
    /// </summary>
    Pathfinder() = default;

    /// <summary>
    /// Construct a pathfinder algorithm and set the movement direction.
    /// </summary>
    Pathfinder(bool diagonalMovement)
    {
        setDiagonalMovement(diagonalMovement);
    };

    /// <summary>
    /// Construct a path from source to destionation.
    /// </summary>
    /// <returns>A path from source to destionation.</returns>
    const std::vector<Coordinate2D>& getReconstructedPath() override
    {
        if (m_isDestFound)
        {
            while (m_current != nullptr)
            {
                m_path.push_back(m_current->getCoordinates());
                m_current = static_cast<TNode*>(m_current->getParent());
            }
        }
        return m_path;
    }

    /// <summary>
    /// Enumerates the coordinates of the explored grid blocks.
    /// </summary>
    /// <param name="f">Function that implements a way to draw explored grid blocks.</param>
    void enumerateClosedNodeCoords(std::function<void(Coordinate2D)> f) override
    {
        for (const auto* node : m_closedSet)
        {
            f(node->getCoordinates());
        }
    }

    /// <summary>
    /// Returns the current node coordinates if it's found.
    /// </summary>
    /// <returns>Coordinates of the current node.</returns>
    Coordinate2D getCurrentNodeCoords() override
    {
        return (m_current) ? m_current->getCoordinates() : Coordinate2D(-1, -1);
    }

    /// <summary>
    /// Free the memory and reset all private members to default.
    /// </summary>
    void clear() override
    {
        for (auto it = m_openSet.begin(); it != m_openSet.end();) {
            delete* it;
            it = m_openSet.erase(it);
        }

        for (auto it = m_closedSet.begin(); it != m_closedSet.end();) {
            delete* it;
            it = m_closedSet.erase(it);
        }

        if (m_current != nullptr)
            delete m_current;

        m_source = Coordinate2D(-1, -1);
        m_dest = Coordinate2D(-1, -1);

        m_path.clear();
        m_isDestFound = false;
    }

protected:
    std::set<TNode*> m_openSet;
    std::set<TNode*> m_closedSet;
    TNode* m_current = nullptr;
};

#endif // !PATHFINDER_H