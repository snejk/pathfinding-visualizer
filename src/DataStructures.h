#ifndef DATA_STRUCTURES_H
#define DATA_STRUCTURES_H
#pragma once

#include <SFML/Graphics.hpp>

#include <Constants.h>

#include <set>
#include <unordered_map>


/// <summary>
/// A structure that represents a node on the grid.
/// </summary>
struct Coordinate2D
{
    Coordinate2D()
    {
        x = -1; 
        y = -1;
    };
    Coordinate2D(int x, int y) : x(x), y(y) {};

    int x;
    int y;
};

inline bool operator>(const Coordinate2D& point, int cmp)
{
    return point.x > cmp && point.y > cmp;
}

inline bool operator<(const Coordinate2D& point, int cmp)
{
    return point.x < cmp && point.y < cmp;
}

inline bool operator>=(const Coordinate2D& point, int cmp)
{
    return point.x >= cmp && point.y >= cmp;
}

inline bool operator<=(const Coordinate2D& point, int cmp)
{
    return point.x <= cmp && point.y <= cmp;
}

inline bool operator==(const Coordinate2D& firstPoint, const Coordinate2D& secondPoint)
{
    return firstPoint.x == secondPoint.x && firstPoint.y == secondPoint.y;
}

inline Coordinate2D operator+(const Coordinate2D& firstPoint, const Coordinate2D& secondPoint)
{
    return Coordinate2D(firstPoint.x + secondPoint.x, firstPoint.y + secondPoint.y);
}

struct equalFunc
{
    bool operator()(const Coordinate2D& firstPoint, const Coordinate2D& secondPoint) const
    {
        return firstPoint.x == secondPoint.x && firstPoint.y == secondPoint.y;
    }
};

struct hashFunc
{
    size_t operator()(const Coordinate2D& coordinate) const
    {
        size_t h1 = std::hash<int>()(coordinate.x);
        size_t h2 = std::hash<int>()(coordinate.y);

        return h1 ^ (h2 << 1);
    }
};

template<class TNode>
bool operator<(const TNode& firstPoint, const TNode& secondPoint)
{
    return std::make_pair(firstPoint.getCoordinates().x, firstPoint.getCoordinates().y) < std::make_pair(secondPoint.getCoordinates().x, secondPoint.getCoordinates().y);
}

/// <summary>
/// A structure that represents a grid block.
/// </summary>
struct Block
{
    sf::RectangleShape m_shape;

    Block() {};
    Block(const Block& other) : Block((int) (other.m_shape.getPosition().x / blockWidth), (int) (other.m_shape.getPosition().y / blockHeight), other.m_shape.getFillColor(), other.m_shape.getOutlineColor()) {};

    Block(sf::Color fillColor, sf::Color outlineColor)
    {
        m_shape.setSize({ blockWidth, blockHeight });
        m_shape.setFillColor(fillColor);
        m_shape.setOutlineColor(outlineColor);
        m_shape.setOutlineThickness(outlineThickness);
    }

    Block(int x, int y, sf::Color fillColor, sf::Color outlineColor) : Block(fillColor, outlineColor)
    {
        m_shape.setPosition(x * blockWidth, y * blockHeight);
        m_shape.setOrigin(0.f, 0.f);
    }
};

using BlockMap = std::unordered_map<Coordinate2D, Block, hashFunc, equalFunc>;
using BlockPair = std::pair<Coordinate2D, Block>;

#endif // !DATA_STRUCTURES_H