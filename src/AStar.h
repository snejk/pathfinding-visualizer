#ifndef ASTAR_H
#define ASTAR_H
#pragma once

#include <PathfinderBase.h>
#include <Pathfinder.h>
#include <Node.h>

/// <summary>
/// A class that represents the Node for the A* pathfinding algorithm.
/// </summary>
class AStarNode : public Node
{
public:

    /// <summary>
    /// Instantiates an AStarNode object.
    /// </summary>
    /// <param name="coordinates">Location of the node.</param>
    /// <param name="parent">Optional parent Node.</param>
    /// <returns>An AStarNode object.</returns>
    AStarNode(const Coordinate2D&, Node*);

    /// <summary>
    /// Total cost of the current node. Sum of G and H.
    /// </summary>
    /// <returns>Total cost of the current node.</returns>
    unsigned int F() const;

    /// <summary>
    /// Get the distance between the current and the source node.
    /// </summary>
    /// <returns>The distance between the current and the source node.</returns>
    unsigned int getG() const;

    /// <summary>
    /// Set the distance between the current and the source node.
    /// </summary>
    /// <param name="g">The distance between the current and the source node.</param>
    void setG(unsigned int);

    /// <summary>
    /// Get the estimated distance between the current and the destination node.
    /// </summary>
    /// <returns>The estimated distance between the current and the destination node.</returns>
    unsigned int getH() const;

    /// <summary>
    /// Set the estimated distance between the current and the destination node.
    /// </summary>
    /// <param name="h">The estimated distance between the current and the destination node.</param>
    void setH(unsigned int);

protected:
    unsigned int m_G = 0;
    unsigned int m_H = 0;
};

/// <summary>
/// A class that represents the A* pathfinding algorithm.
/// </summary>
class AStar : public Pathfinder<AStarNode>
{
public:

    /// <summary>
    /// Instantiates an A* algorithm object.
    /// </summary>
    /// <param name="diagonalMovement">Enable the diagonal movement for the algorithm.</param>
    /// <returns>And object representing the A* algorithm.</returns>
    AStar(bool);
    ~AStar();

    /// <summary>
    /// Get the name of the current algorithm.
    /// </summary>
    /// <returns>The current algorithm's name.</returns>
    sf::String getName() override;

    /// <summary>
    /// The main method of the algorithm. Explores neighbours to find the path to the destination.
    /// </summary>
    /// <param name="wallBlocks">A map of wall grid blocks.</param>
    void explorePath(const BlockMap&) override;

    /// <summary>
    /// Sets the source block's coordinates.
    /// </summary>
    /// <param name="newSource">Coordinates of the new source block.</param>
    void setSource(Coordinate2D) override;

private:
    std::set<AStarNode*> m_openSet;
    static std::pair<int, int> calcDelta(const Coordinate2D&, const Coordinate2D&);
    static int manhattan(const Coordinate2D&, const Coordinate2D&);
    static int euclidean(const Coordinate2D&, const Coordinate2D&);
    static int octagonal(const Coordinate2D&, const Coordinate2D&);
};

#endif // !ASTAR_H