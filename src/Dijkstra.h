#ifndef DIJKSTRA_H
#define DIJKSTRA_H
#pragma once

#include <PathfinderBase.h>
#include <Pathfinder.h>
#include <Node.h>

/// <summary>
/// A class that represents the Node for the Dijkstra pathfinding algorithm.
/// </summary>
class GraphNode : public Node
{
public:

    /// <summary>
    /// Instantiates a GraphNode object.
    /// </summary>
    /// <param name="coordinates">Location of the node.</param>
    /// <param name="parent">Optional parent Node.</param>
    /// <returns>A GraphNode object.</returns>
    GraphNode(const Coordinate2D&, Node*);

    /// <summary>
    /// Get the distance between the current and the source node.
    /// </summary>
    /// <returns>The distance between the current and the source node.</returns>
    unsigned int getDistance() const;

    /// <summary>
    /// Set the distance between the current and the source node.
    /// </summary>
    /// <param name="distance">The distance between the current and the source node.</param>
    void setDistance(unsigned int);

protected:
    unsigned int m_distance = 0;
};


/// <summary>
/// A class that represents the Dijkstra pathfinding algorithm.
/// Breadth first search and Dijsktra perform the same in this case
/// because the distance between nodes is the same.
/// </summary>
class Dijkstra : public Pathfinder<GraphNode>
{
public:

    /// <summary>
    /// Instantiates a Dijkstra algorithm object.
    /// </summary>
    /// <param name="diagonalMovement">Enable the diagonal movement for the algorithm.</param>
    /// <returns>And object representing the Dijkstra algorithm.</returns>
    Dijkstra(bool);
    ~Dijkstra();

    /// <summary>
    /// Get the name of the current algorithm.
    /// </summary>
    /// <returns>The current algorithm's name.</returns>
    sf::String getName() override;

    /// <summary>
    /// The main method of the algorithm. Explores neighbours to find the path to the destination.
    /// </summary>
    /// <param name="wallBlocks">A map of wall grid blocks.</param>
    void explorePath(const BlockMap&) override;

    /// <summary>
    /// Sets the source block's coordinates.
    /// </summary>
    /// <param name="newSource">Coordinates of the new source block.</param>
    void setSource(Coordinate2D) override;

private:
    std::set<GraphNode*> m_openSet;
};

#endif // !DIJKSTRA_H