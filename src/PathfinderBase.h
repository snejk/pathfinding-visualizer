#ifndef PATHFINDER_BASE_H
#define PATHFINDER_BASE_H
#pragma once

#include <SFML/Graphics.hpp>

#include <Constants.h>
#include <DataStructures.h>

#include <vector>
#include <functional>

/// <summary>
/// An abstract class represeting a generic pathfinding algorithm.
/// Cannot be instantiated and can be used for implementing algorithms.
/// </summary>
class PathfinderBase
{
public:
    /// <summary>
    /// Pure virtual method for implementing a way to get the path from source to destination.
    /// </summary>
    /// <returns>A reference to the reconstructed path from source to destination.</returns>
    virtual const std::vector<Coordinate2D>& getReconstructedPath() = 0;

    /// <summary>
    /// Pure virtual method for implementing cleanup after the algorithm has finished.
    /// </summary>
    virtual void clear() = 0;

    /// <summary>
    /// Pure virtual method for implementing a pathfinding algorithm core.
    /// </summary>
    /// <param name="wallBlocks">A map of wall collision blocks.</param>
    virtual void explorePath(const BlockMap& wallBlocks) = 0;

    /// <summary>
    /// Pure virtual method for implementing a way to get the algorithm name.
    /// </summary>
    /// <returns>A string containing the algorithm name.</returns>
    virtual sf::String getName() = 0;

    /// <summary>
    /// Pure virtual method for implementing a way to get coordinates of the already explored grid blocks.
    /// </summary>
    /// <param name="f">Function that implements a way to draw explored grid blocks.</param>
    virtual void enumerateClosedNodeCoords(std::function<void(Coordinate2D)> f) = 0;

    /// <summary>
    /// Pure virtual method for implementing a way to get coordinates of the current node.
    /// </summary>
    /// <returns>Coordinates of the current node.</returns>
    virtual Coordinate2D getCurrentNodeCoords() = 0;

    /// <summary>
    /// Set the coordinates for the source block.
    /// </summary>
    /// <param name="newSource">Coordinates for setting the source block.</param>
    virtual void setSource(Coordinate2D);

    /// <summary>
    /// Set the coordinates for the destination block.
    /// </summary>
    /// <param name="newDest">Coordinates for setting the destination block.</param>
    virtual void setDest(Coordinate2D);

    /// <summary>
    /// Get the coordinates of the source block.
    /// </summary>
    /// <returns>Coordinates of the source block.</returns>
    virtual Coordinate2D getSource();

    /// <summary>
    /// Get the coordinates of the destination block.
    /// </summary>
    /// <returns>Coordinates of the destination block</returns>
    virtual Coordinate2D getDest();

    /// <summary>
    /// Returns true if the algorithm has found the way from source to destination.
    /// </summary>
    /// <returns>Whether the destination was found or not.</returns>
    bool isDestFound() const;

    /// <summary>
    /// Enable or disable diagonal movement.
    /// </summary>
    /// <param name="enable">Enables diagonal movement if set to true.</param>
    void setDiagonalMovement(bool);

    /// <summary>
    /// Enumerator for setting the direction in which the algorithm can explore the grid.
    /// </summary>
    enum class Directions : unsigned int
    {
        ORTHOGONAL = 4,
        DIAGONAL = 8
    };

protected:
    std::vector<Coordinate2D> m_path;
    Coordinate2D m_source;
    Coordinate2D m_dest;
    bool m_isDestFound = false;
    unsigned int m_directionCount = (unsigned int) Directions::DIAGONAL;

    static const std::vector<Coordinate2D> directions;
    static constexpr unsigned int orthogonalCost = 10;
    static constexpr unsigned int diagonalCost = 14; // Calculated as sqrt(orthogonalCost^2 + orthogonalCost^2)

    static bool isTraversable(const Coordinate2D, const BlockMap&);
};

#endif // !PATHFINDER_BASE_H
