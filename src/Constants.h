#ifndef CONSTANTS_H
#define CONSTANTS_H
#pragma once

constexpr unsigned int gridWidth = 800;
constexpr unsigned int gridHeight = 800;

constexpr unsigned int xBlockCount = 20;
constexpr unsigned int yBlockCount = 20;

constexpr float blockWidth = gridWidth / xBlockCount;
constexpr float blockHeight = gridHeight / yBlockCount;

constexpr float outlineThickness = -0.5f;

#endif // !CONSTANTS_H