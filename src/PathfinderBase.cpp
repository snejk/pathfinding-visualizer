#include <PathfinderBase.h>

void PathfinderBase::setSource(Coordinate2D newSource)
{
    m_source = newSource;
}

void PathfinderBase::setDest(Coordinate2D newDest)
{
    m_dest = newDest;
}

Coordinate2D PathfinderBase::getSource()
{
    return m_source;
}

Coordinate2D PathfinderBase::getDest()
{
    return m_dest;
}

bool PathfinderBase::isDestFound() const
{
    return m_isDestFound;
}

void PathfinderBase::setDiagonalMovement(bool enable = true)
{
    m_directionCount = (unsigned int) (enable ? Directions::DIAGONAL : Directions::ORTHOGONAL);
}

bool PathfinderBase::isTraversable(const Coordinate2D coordinate, const BlockMap& wallBlocks)
{
    // Not traversable if the current node is inside a wall or if it's out of the grid's bounds
    if (wallBlocks.count(coordinate) ||
        coordinate.x < 0 || coordinate.y < 0 ||
        coordinate.x >= xBlockCount || coordinate.y >= yBlockCount)
    {
        return false;
    }

    return true;
}

const std::vector<Coordinate2D> PathfinderBase::directions = {
        { -1, 0 }, { 0, 1 }, { 1, 0 }, { 0, -1 },   // orthogonal movement
        { -1, -1 }, { 1, 1 }, { -1, 1 }, { 1, -1 } // diagonal movement
};