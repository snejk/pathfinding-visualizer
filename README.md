## 2D grid pathfinding visualizer

A C++ 2D grid pathfinding visualizer using SFM library

Available algorithms:
 - Depth first search
 - Breadth first search / Dijkstra
 - A*

### Installation

1. Install libsfml-dev with your package manager, eg. `sudo apt-get install libsfml-dev`
2. Clone the repository.
3. Run `cmake .`
4. Run `make`
5. Execute the binary with `./pathfinding`

### Usage

The following hotkeys are available:
 - S - set the source block on the grid
 - D - set the destionation block on the grid
 - R - reset the whole grid
 - G - run the visualizer
 - A - cycle through algorithms (current algorithms is shown in the window title)
 - Up arrow - speed up the animation
 - Down arrow - slow down the animation
 - Hold and drag the left mouse button to draw collision blocks
 - Hold and drag the right mouse button to erase collision blocks

### Preview

[](https://i.imgur.com/7ZaAoAf.gif)
[](https://i.imgur.com/aLEqVXH.gif)
<img src="https://i.imgur.com/7ZaAoAf.gif" width="400" height="400">
<img src="https://i.imgur.com/aLEqVXH.gif" width="400" height="400">